#!/usr/bin/env node
require('colors');
const zmq = require("zeromq");
const config = require("./config");
const zlib = require('zlib');
const mariadb = require('mariadb');
const Curve = require("./lib/Curve");
const EntityModel = require("./lib/EntityModel");
const websocket = require("ws");

const serviceName = 'Business service';

function getConnectionString(config) {
    return `${config.server.transport}://${config.server.host}${config.server.port ? ':' + config.server.port : ''}`;
}

try {
    // init websocket
    const ws = new websocket(config.websocket.address);

    // init db
    const mariadbPool = mariadb.createPool({
        host: config.db.host,
        port: config.db.port,
        user: config.db.user,
        password: config.db.password,
        database: config.db.database,
        connectionLimit: config.db.connectionLimit
    });

    // init zeromq
    const curve = new Curve({name: serviceName});
    const sub = new zmq.Subscriber;

    sub.curvePublicKey = curve.getKeyFromFile("public_key.curve");
    sub.curveSecretKey = curve.getKeyFromFile("secret_key.curve");
    sub.curveServerKey = curve.getKeyFromFile("server_public_key.curve");

    (async () => {
        await sub.connect(getConnectionString(config));
        sub.subscribe(config.server.channel);
        console.info(`${Date()} > ${serviceName} - connected to pub.`.cyan.bold);

        for await (const [channel, message] of sub) {
            let dbConnection = await mariadbPool.getConnection();
            await zlib.inflate(message, (e, buffer) => {
                if (!e) {
                    let data = JSON.parse(buffer.toString('utf8'));
                    if (!dbConnection || !dbConnection.isValid()) {
                        throw new Error(`${Date()} > ${serviceName} - db connection lost!`);
                    }
                    (new EntityModel(dbConnection, data)).save();
                    if (ws.readyState === websocket.OPEN) {
                        ws.send(JSON.stringify(data));
                    }
                } else {
                    throw new Error(`${Date()} > ${serviceName} - zlib error due decompress: ${e}`);
                }
            });
            await dbConnection.end();
        }
    })();

} catch (e) {
    console.error(`${Date()} > ${serviceName} - error: ${e.message}`);
}
