const fs = require('fs');
const path = require("path");

class Curve {
    constructor(options) {
        this.name = options.name ? options.name : false;
        this.sep = path.sep;
        this.dir = options.dir ? options.dir : path.dirname(require.main.filename) + `${this.sep}.keys`;
    }

    /**
     * @param fileName
     * @returns {string}
     */
    getKeyFromFile(fileName) {
        if (fs.existsSync(this.dir + this.sep + fileName)) {
            return fs.readFileSync(this.dir + this.sep + fileName, 'utf8').split(/\r?\n/)[0];
        }
    }

    /**
     * @param fileName
     * @param key
     */
    createKeyFile(fileName, key) {
        fs.appendFileSync(this.dir + this.sep + fileName, key + "\n", "utf8");
        console.info(`${Date()} > ${this.name ? this.name + ' - ' : ''}New file '${fileName}' key created!`);
    }
}

module.exports = Curve;
