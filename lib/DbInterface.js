class DbInterface {
    //  database instance
    #connection;

    /**
     * @param dbConnection
     */
    constructor(dbConnection) {
        this.#connection = dbConnection;
    }

    /**
     * @param string
     * @param params
     * @returns {Promise<*|PermissionStatus>}
     * @protected
     */
    async query(string, params = []) {
        return this.#connection.query(string, params);
    }

    /**
     * @param string
     * @param batch
     * @returns {Promise<[]>}
     * @protected
     */
    async batch(string, batch) {
        return this.#connection.batch(string, batch);
    }

    /**
     * @param attribute
     * @returns {boolean}
     * @protected
     */
    check(attribute) {
        return !(attribute === '' || attribute === null || attribute === undefined);
    }
}

module.exports = DbInterface;
