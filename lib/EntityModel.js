const DbInterface = require("./DbInterface");

class EntityModel extends DbInterface {
    // table attributes
    attributes = {};

    /**
     * @param dbConnection
     * @param attributes
     */
    constructor(dbConnection, attributes = {}) {
        super(dbConnection);
        this.createAttributes(attributes);
    }

    /**
     * @param attributes
     */
    createAttributes(attributes) {
        for (let [key, value] of Object.entries(attributes)) {
            this.attributes[key] = value;
        }
    }

    /**
     * @returns {boolean}
     */
    async save() {
        const attributes = Object.entries(this.attributes);
        const names = attributes.map(([key]) => {
            return key;
        }).join(",");
        const placeHolders = attributes.map(() => {
            return '?';
        }).join(",");
        const updateQuery = attributes.map(([key, value]) => {
            return `${key}=${value}`;
        }).join(",").replace(/,id.+/g, '');
        const result = await this.query(`INSERT INTO entity (${names}) VALUES (${placeHolders}) ON DUPLICATE KEY UPDATE ${updateQuery}`,
            attributes.map(([key, value]) => {
                return value;
            }));
        return !!result.affectedRows;
    }
}

module.exports = EntityModel;
